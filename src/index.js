'use strict'

const fs = require ('fs');
const os = require ('os');
const path = require ('path');
const langs = require("linguist-languages");
const csvParse = require ('csv-parse/sync');
const fg = require('fast-glob');
const mdescape = require('markdown-escape');
const yamlFront = require('yaml-front-matter');

function pandemicsInclude (content, options = {}) {

  // default options
  options.basePath = options.basePath || process.env.PANDOC_SOURCE_PATH || process.cwd()
  options.relPath = options.relPath || '.'
  options.patternStart = options.patternStart || '({{|<!--)';
  options.patternStop = options.patternStop || '(}}|-->)';
  options.errorLogger = errorDiv;

  // front matter
  const frontMatter = yamlFront.loadFront (content);


  // check and rebase images
  // http://blog.michaelperrin.fr/2019/02/04/advanced-regular-expressions/
  let imgPattern = /!\[(?<alt>.*)\]\(\s*(?<url>[^"\)]+)\b\s*(?:"(?<title>.+)")?\)/g
  content = replaceUnlessVerbatim(content, imgPattern, (...args) => {
    let {alt, url, title} = args.pop();
    if (!path.isAbsolute(url)) {
      url = path.join (options.relPath, url)
    }
    let imagePath = path.join (options.basePath, url)
    if (!fs.existsSync (imagePath) || !fs.statSync (imagePath).isFile ()) {
      return options.errorLogger (new Error(`**Image:** cannot find *${mdescape(url)}*`))
    }
    title = title ? ` "${title}"` : ""
    return `![${alt}](${url}${title})`
  })

  // {{ #type path/to/my file.ext param1=val1 param2=val2 }}
  var pattern= new RegExp([
    /({{|<!--)/ // begining, {{ or <!--
    ,/\s*/
    ,/#(?<type>\w+)/ // #type
    ,/\s+/
    ,/(?<file>[^=}]+)\b/ // file path, accept everything untill params or end
    ,/(\s+(?<params>(?:[^=\s]+=[^=\s]+\s*)+)\b\s*)?/ // pairs of key=val
    ,/\s*/
    ,/(}}|-->)/ // end, }} or -->
  ].map(r => r.source).join(''));

  // replace matches with the corresponding inclusions
  return replaceUnlessVerbatim(content, pattern, (...args) => {

    // parse elements of the inclusion command
    let {type, file, params = ''} = args.pop();
    params = Object.fromEntries(params.split(/\s+/).map(p => p.split(/=/)));

    // returned inclusion according to type
    try {
      switch(type) {
        case 'cite':
          return includeCite (file, options, params);
          break;
        case 'code':
          return includeCode (file, options, params);
          break;
        case 'csv':
          return includeCsv (file, options, params);
          break;
        case 'include':
          let fileList = fg.sync(file, {cwd: options.basePath}).filter(f=>f.endsWith('.md'))
          if (!fileList.length) {
            throw new Error(`no files matching: *${mdescape(file)}*`)
          }
          return fileList.map(f => {
            return includeMarkdown (f, options, params)
          }).join('\n')
          break;
        default:
          throw new Error(`unknown request: *${mdescape(type)}*`)
      }
    // fail gracefully (default as fenced div)
    } catch (err) {
      if (frontMatter.pandemics && frontMatter.pandemics.strict) {
        throw err
      } else {
        let label = {cite: 'Citation', code: 'Listing', csv: 'Table', include: 'Include'}[type] || 'Error'
        err.message = `**${label}:** ${err.message}`
        return options.errorLogger (err)
      }
    }
  }).replace(/\n{3,}/gm,`\n\n`); // cosmetic only: avoid too much spacing
}

function readFile (file, options) {
    var filePath = path.join (options.basePath, options.relPath, file.trim ());
    if (!fs.existsSync (filePath) || !fs.statSync (filePath).isFile ()) {
        throw new Error(`cannot find *${mdescape(file)}*.`);
    }
    // todo: use params to load different file encoding
    return fs.readFileSync (filePath, 'utf8');
};

function replaceUnlessVerbatim (text, oldString, newString) {
  // block detection: keep track line by line if we are inside a code block
  let mdBlock = false;
  return text.split ('\n').map ( line => {
    // begin or end of block, switch flag
    if (line.trim ().startsWith ('```')) {
      mdBlock = !mdBlock;
    }
    // inside blocks, return unchanged string
    if (mdBlock) {
      return line;
    // not in a code block
    } else {
      // detect inline code
      let codePattern = /(`(?:[^`]|(?<=\\)`)+(?<!\\)`)/g
      // go over parts of the line, if code ignore, otherwise replace
      return line.split (codePattern).map (substr => {
        return substr.match(codePattern) ? substr : substr.replace (oldString, newString)
      }).join('');
    }
  }).join('\n')
}

// use fenced div to allow errors to be displayed directly inside the output document
function errorDiv (err) {
    return `\n\n::: pandemics-include-error\n${err.message}\n:::\n\n`
}

function includeCode (file, options, params) {
    // read file content
    let source = readFile (file, options);
    // try to guess syntax from linguist if not explicitly required
    if (! params.syntax) {
      // find syntaxes using Included file extension
      let ext = path.extname(file).toLowerCase()
      let candidates = Object.keys(langs).filter(l => (langs[l].extensions || []).includes(ext));
      // only use guess if unambiguous
      if (candidates.length == 1) {
        params.syntax = candidates[0].toLowerCase();
      } else {
        params.syntax = '';
      }
    }
    // return code in fenced block
    return '```' + params.syntax + os.EOL + source.trim () + os.EOL + '```';
};

 function includeCite (file, options, params) {
     // read file content
     let source = readFile (file, options);
     // prepend >
     return source.replace (/^/gm, '> ')
};

function includeMarkdown (file, options, params) {
    // read file content
    let source = readFile(file, options);

    // recurse
    let optionsInclude = Object.assign ({}, options);
    optionsInclude.relPath = path.join(options.relPath, path.dirname(file));
    return pandemicsInclude (source, optionsInclude)
};

function includeCsv (file, options, params) {

  params.columns = false;
  let source = readFile(file, options);
  const data = csvParse.parse (source, params);

  var header
  if (JSON.parse(params.header || true)) {
    header = data.splice(0,1)[0];
    header = header.map(t => /^\s*$/.test (t.toString()) ? ' ' : `**${t.toString().trim()}**`);
  } else {
    header = data[0].map (e => ' ');
  }

  var separator = header.map(t => '---');

  // cell size
  if (params.width) {
    let dims = params.width.split(/[^\d]/).map(n => parseInt(n));
    separator = separator.map((item, i) => item.repeat(dims[i]))
  }

  // cell alignment
  if (params.align) {
    separator = separator.map((item, i) => {
      let a = params.align[i];
      return item
        .replace(/^./, a != 'r' ? ':' : '-')
        .replace(/.$/, a != 'l' ? ':' : '-')
    });
  }

  return '' +
    '| ' + header.join(' | ') + ' |\n' +
    '| ' + separator.join(' | ') + ' |\n' +
    data.map (row => '| ' + row.map (cell => cell.toString().trim()).join(' | ') + ' |').join('\n')

}


module.exports = pandemicsInclude
