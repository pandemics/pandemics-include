#!/usr/bin/env node
const fs = require ('fs');
const path = require ('path');
const pandemicsInclude = require('./index.js')

// read from file passed as arg, or stdin by default
var input = process.argv[2] || 0;

// process
var options = {}
if (process.argv[2]) {
  options.basePath = path.resolve (process.cwd (), path.dirname (process.argv[2]))
}

try {
  var content = pandemicsInclude (fs.readFileSync (input, 'utf8'), options);
  // write to the pipe
  process.stdout.write (content);
} catch (err) {
  console.error (`*** pandemics-include: ${err.message}`)
}
