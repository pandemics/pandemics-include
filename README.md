# pandemics-include

Allows to include markdown, text, or csv files directly in your manuscript.

### Include other markdown document:

```markdown
{{ #include chapters/one.md }}
```

Note that you can also use jockers:

```markdown
{{ #include chapters/*.md }}
```

### Include csv file (will be converted to Markdown table, neat!):

```markdown
{{ #csv data/values.csv }}
```
You can also pass on options to change the loading or display of the table, eg:

```markdown
{{ #csv data/values.csv align=lcr colsize=2:1:1 }}
```

Options are:

- `align`: a sequence of `l`, `c`, or `r` to align each column to respectively to the left, center, or right.
You must provide as many flags are there are columns in the table.
- `colsize`: relative size of the columns, when the table is too large and has to be displayed in full page width.
You must specify one number per column, separated by `:`. For example, `1:3` will scale two columns to take 25% and 75% of the page width respectively.
- any option described in the [csv-parse documentation](https://csv.js.org/parse/options/), passed as `key=value`.

### Include source code:

```markdown
{{ #code example.js }}
```

The syntax highlighting will be guessed from the file extension, you can however force the language syntax:

```markdown
{{ #code example.js syntax=javascript }}
```

### Include as citation (prefixed with `>`):

```markdown
{{ #cite quotes/einstein.txt }}
```
