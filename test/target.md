## subsection

| **a** | **b** | **c** |
| --- | --- | --- |
| 1 | 2 | 3 |

::: pandemics-include-error
**Table:** cannot find *includes\/no-table.csv*.
:::

![](sub/includes/image.svg)

::: pandemics-include-error
**Image:** cannot find *sub\/includes\/no-image.svg*
:::

::: pandemics-include-error
**Include:** no files matching: *sub\/no-subsection.md*
:::

> This is an included text.

::: pandemics-include-error
**Citation:** cannot find *nofile.txt*.
:::

